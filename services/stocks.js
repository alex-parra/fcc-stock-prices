const db = require('../db.js');
const { stocksApi } = require('../services/stocksApi.js');

/**
 * @param {string[]} stocks Lower case stock tickers to load
 * @retuns Promise[]
 */
const loadStocks = stocks => {
  if( !Array.isArray(stocks) ) throw new TypeError('Argument stocks must be string[]');
  return Promise.all(stocks.map(ticker => db.firstOrCreate('stocks', {stock: ticker}, {likes: []})));
}

const likeStocks = async (stocks, ip) => {
  const dbStocks = await loadStocks(stocks);
  const promises = dbStocks.map(async dbStock => {
    const likes = dbStock.likes.filter(l => l.ip !== ip).concat({ ip });
    return await db.update('stocks', {_id: db.id(dbStock._id)}, {likes});
  });
  await Promise.all(promises);
}

const getStocks = async stocks => {
  const dbStocks = await loadStocks(stocks);
  return await Promise.all(dbStocks.map(async dbStock => {
    const apiData = await stocksApi(dbStock.stock);
    return { stock: apiData.symbol, price: apiData.latestPrice.toFixed(2), likes: dbStock.likes.length };
  }));
}

module.exports = {
  loadStocks,
  likeStocks,
  getStocks
}