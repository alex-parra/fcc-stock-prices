const fetch = require('node-fetch');

const stocksApi = ticker => {
  return fetch(`https://repeated-alpaca.glitch.me/v1/stock/${ticker}/quote`).then(r => r.json());
};

module.exports = {
  stocksApi
}