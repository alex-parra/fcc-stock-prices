const { getStocks, likeStocks } = require('../services/stocks.js');

const getStock = async (req, res) => {
  let { stock, like } = req.query;
  if (!stock) return res.status(400).json('Stock symbol is required');
  
  const stocks = (!Array.isArray(stock) ? [stock] : stock).map(ticker => ticker.toLowerCase());

  try {
    if( like ) await likeStocks(stocks, req.ip);
    const stockData = await getStocks(stocks);
    
    if( stockData.length === 1 ) return res.json({stockData: stockData[0]});
    
    const stock1 = stockData[0];
    const stock2 = stockData[1];
    
    stock1.rel_likes = stock1.likes - stock2.likes;
    stock2.rel_likes = stock2.likes - stock1.likes;
    delete stock1.likes;
    delete stock2.likes;
    
    return res.json({ stockData });
    
  } catch (error) {
    return res.status(400).json(error.message || 'Get stock info failed');
  }
};

module.exports = {
  getStock,
};
