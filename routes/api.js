/*
 *
 *
 *       Complete the API routing below
 *
 *
 */

'use strict';

const StockPrices = require('../controllers/StockPrices');

module.exports = function(app) {
  app.route('/api/stock-prices').get(StockPrices.getStock);
};
