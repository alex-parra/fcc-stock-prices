/*
 *
 *
 *       FILL IN EACH FUNCTIONAL TEST BELOW COMPLETELY
 *       -----[Keep the tests in the same order!]-----
 *       (if additional are added, keep them at the very end!)
 */

var chaiHttp = require('chai-http');
var chai = require('chai');
var assert = chai.assert;
var server = require('../server');

chai.use(chaiHttp);

const { loadStocks } = require('../services/stocks');

suite('Functional Tests', function() {
  suite('GET /api/stock-prices => stockData object', function() {
    test('1 stock', function(done) {
      chai
        .request(server)
        .get('/api/stock-prices')
        .query({ stock: 'goog' })
        .end(function(err, res) {
          assert.equal(res.status, 200);
          assert.isObject(res.body);
          assert.property(res.body, 'stockData');
          assert.isObject(res.body.stockData);
          assert.property(res.body.stockData, 'stock');
          assert.property(res.body.stockData, 'price');
          assert.property(res.body.stockData, 'likes');
          assert.notProperty(res.body.stockData, 'rel_likes');
          done();
        });
    });

    test('1 stock with like', function(done) {
      loadStocks(['aapl']).then(([dbStock]) => {
        chai
          .request(server)
          .get('/api/stock-prices')
          .query({ stock: 'aapl', like: 0 })
          .end(function(err, res) {
            assert.equal(res.status, 200);
            assert.equal(res.body.stockData.likes, dbStock.likes.length + 1);
            done();
          });
      });
    });

    test('1 stock with like again (ensure likes arent double counted)', function(done) {
      loadStocks(['aapl']).then(([dbStock]) => {
        chai
          .request(server)
          .get('/api/stock-prices')
          .query({ stock: 'aapl', like: 1 })
          .end(function(err, res) {
            assert.equal(res.status, 200);
            assert.equal(res.body.stockData.likes, dbStock.likes.length);
            done();
          });
      });
    });

    test('2 stocks', function(done) {
      chai
        .request(server)
        .get('/api/stock-prices')
        .query({ stock: ['goog', 'msft'] })
        .end(function(err, res) {
          assert.equal(res.status, 200);
          assert.isObject(res.body);
          assert.property(res.body, 'stockData');
          assert.isArray(res.body.stockData);
          assert.equal(res.body.stockData.length, 2);
          assert.property(res.body.stockData[0], 'stock');
          assert.property(res.body.stockData[0], 'price');
          assert.notProperty(res.body.stockData[0], 'likes');
          assert.property(res.body.stockData[0], 'rel_likes');
          done();
        });
    });

    test('2 stocks with like', function(done) {
      loadStocks(['goog', 'msft']).then(dbStocks => {
        chai
          .request(server)
          .get('/api/stock-prices')
          .query({ stock: ['goog', 'msft'], like: 1 })
          .end(function(err, res) {
            assert.equal(res.status, 200);
            assert.isObject(res.body);
            assert.property(res.body, 'stockData');
            assert.isArray(res.body.stockData);
            assert.equal(res.body.stockData.length, 2);
            assert.property(res.body.stockData[0], 'stock');
            assert.property(res.body.stockData[0], 'price');
            assert.notProperty(res.body.stockData[0], 'likes');
            assert.property(res.body.stockData[0], 'rel_likes');
            assert.property(res.body.stockData[1], 'stock');
            assert.property(res.body.stockData[1], 'price');
            assert.notProperty(res.body.stockData[1], 'likes');
            assert.property(res.body.stockData[1], 'rel_likes');
            done();
          });
      });
    });
  });
});
