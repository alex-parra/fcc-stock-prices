/*
 *
 *
 *       FILL IN EACH UNIT TEST BELOW COMPLETELY
 *       -----[Keep the tests in the same order!]----
 *       (if additional are added, keep them at the very end!)
 */

var chai = require('chai');
const assert = chai.assert;

const { stocksApi } = require('../services/stocksApi.js');
const { loadStocks, getStocks } = require('../services/stocks.js');

suite('Unit Tests', function() {
  
  test('Stocks API', function(done) {
    stocksApi('aapl').then(data => {
      assert.property(data, 'symbol');
      assert.property(data, 'latestPrice');
      assert.equal(data.symbol, 'aapl'.toUpperCase());
      done();
    });
  });
  
  test('Stocks Service - load single stock', function(done) {
    loadStocks(['aapl']).then(dbStocks => {
      assert.isArray(dbStocks);
      assert.equal(dbStocks.length, 1);
      assert.equal(dbStocks[0].stock, 'aapl');
      done();
    });
  });
  
  test('Stocks Service - load multiple stocks', function(done) {
    loadStocks(['aapl', 'msft']).then(dbStocks => {
      assert.isArray(dbStocks);
      assert.equal(dbStocks.length, 2);
      assert.property(dbStocks[0], 'stock');
      done();
    });
  });
  
  test('Stocks Service - get single stock data', function(done) {
    getStocks(['aapl']).then(dbStocks => {
      assert.isArray(dbStocks);
      assert.equal(dbStocks.length, 1);
      assert.property(dbStocks[0], 'stock');
      assert.property(dbStocks[0], 'price');
      assert.property(dbStocks[0], 'likes');
      done();
    })
  });
  
  test('Stocks Service - get multiple stock data', function(done) {
    getStocks(['aapl', 'msft']).then(dbStocks => {
      assert.isArray(dbStocks);
      assert.equal(dbStocks.length, 2);
      assert.property(dbStocks[0], 'stock');
      assert.property(dbStocks[0], 'price');
      assert.property(dbStocks[0], 'likes');
      done();
    })
  });
  
});
